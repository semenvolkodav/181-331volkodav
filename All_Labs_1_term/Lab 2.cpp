//
//  Lab 2.cpp
//  PolendromLab_181-331_Volkodav
//
//  Created by MAD FOX on 06.10.2018.
//  Copyright © 2018 MAD FOX. All rights reserved.
//

#include "Lab 2.hpp"
#include <iostream>
#include <string>

class Student
{
public://доступные данные
    Student();//конструктор
    ~Student();//деструктор
    Student(int st_group, int st_age, std::string st_sur );
    int age;
    void set_group(int st_group);//метод
    
    int get_group();
    std::string get_sur();
    void set_sur(std::string);
    
    
private://закрытые данные
    std::string surname;
    int group;

    
    
    
    
};

Student::Student(int st_group, int st_age, std::string st_sur) {
    age = st_age;
    group = st_group;
    surname = st_sur;
    
}

Student::Student() {
    
}
Student::~Student() {
}
void Student::set_group(int st_group) {
    group = st_group;
}
int Student::get_group() {
    return group;
}
std::string Student::get_sur(){
    return surname;
}
void Student::set_sur(std::string st_sur){
    surname = st_sur;
}





